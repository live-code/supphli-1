export type Role = 'admin' | 'moderator';

export type Auth = {
  token: string;
  displayName: string;
  role: Role;
}


