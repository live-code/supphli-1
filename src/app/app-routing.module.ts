import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: 'users', loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule) },
      { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule) },
      { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
      { path: 'uikit2', loadChildren: () => import('./features/uikit2/uikit2.module').then(m => m.Uikit2Module) },
      { path: 'forms1', loadChildren: () => import('./features/forms1/forms1.module').then(m => m.Forms1Module) },
      { path: 'forms2', loadChildren: () => import('./features/forms2/forms2.module').then(m => m.Forms2Module) },
      { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
      { path: 'uikit3', loadChildren: () => import('./features/uikit3/uikit3.module').then(m => m.Uikit3Module) }
    ])
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}
