import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-tabbar',
  template: `

    <div class="flex gap-2">
      <button
        *ngFor="let item of items"
        class="btn"
        [ngClass]="{
          'border-2 border-slate-900': item.id === active?.id
        }"

        (click)="tabClickHandler(item)"
      >
        {{item[labelField]}}
      </button>
    </div>
  `,
})
export class TabbarComponent<T extends { id: number, [key: string]: any}> {
  @Input() items: T[] = [];
  @Input() active: T | undefined;
  @Input() labelField: string = 'label'
  @Output() tabClick = new EventEmitter<T>()

  tabClickHandler(item: T) {
    this.tabClick.emit(item)
  }
}


