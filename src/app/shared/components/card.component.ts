import {Component, EventEmitter, Input, Output} from '@angular/core';


@Component({
  selector: 'app-card',
  template: `
    <div>
      <div
        class="text-white p-3 flex justify-between items-center"
        [ngClass]="CLASSES[variant]"
      >
        <div>{{title}}</div>
        <i *ngIf="icon" [class]="icon" (click)="iconClick.emit()"></i>
      </div>
      <div class="border border-black p-3">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title: number | string | undefined
  @Input() icon: string | undefined;
  @Input() url: string | undefined;
  @Input() variant: 'primary' | 'accent' | 'default' = 'default'
  @Output() iconClick = new EventEmitter()

  CLASSES = {
    'default': 'bg-slate-900',
    'primary': 'bg-sky-500',
    'accent': 'bg-pink-500',
  }

}
