import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-mapquest',
  template: `
   <img width="100%"
        [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + city + '&size=1200,800&zoom=' + zoom" alt="">
  `,
  styles: [
  ]
})
export class MapquestComponent {
  @Input() city: string = '';
  @Input() zoom = 10
}
