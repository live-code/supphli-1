import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { debounce, debounceTime, map, mergeMap, of, timer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AsyncUserValidatorService {
  constructor(private http: HttpClient) {

  }

  checkUniqueUsername(): AsyncValidatorFn {
    return (c: AbstractControl) => {
      return timer(1000)
        .pipe(
          mergeMap(() => {
            return this.http.get<any>(`https://jsonplaceholder.typicode.com/users?username=${c.value}`)
              .pipe(
                map(res => res.length > 0 ? { alreadyExist: true} : null)
              )
          })
        )

    }
  }
}
