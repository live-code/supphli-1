import {Directive, ElementRef, Input, OnChanges, Renderer2, SimpleChange, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[appMargin]'
})
export class MarginDirective {
  @Input() set appMargin(val: number) {
    this.renderer.setStyle(
      this.el.nativeElement,
      'margin',
      (val * 2) + 'px'
    )
  };

  @Input() color: string = 'red'

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes['color'])
  }


}
