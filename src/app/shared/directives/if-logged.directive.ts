import { Directive, ElementRef, HostBinding, HostListener, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import {
  combineLatest,
  debounceTime,
  distinctUntilChanged,
  distinctUntilKeyChanged,
  forkJoin,
  map, Subject, Subscription, takeUntil,
  withLatestFrom
} from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { Role } from '../../model/auth';

@Directive({
  selector: '[appIfLogged]'
})
export class IfLoggedDirective {
  @Input()  appIfLogged!: Role

  constructor(
    private tpl: TemplateRef<any>,
    private el: ElementRef,
    private view: ViewContainerRef,
    private authService: AuthService
  ) { }

  sub = new Subject<void>();

  ngOnInit() {
    this.authService.isValid$(this.appIfLogged)
      .pipe(
        takeUntil(this.sub)
      )
      .subscribe((show) => {
       if (show) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear()
        }
      })
  }

  ngOnDestroy() {
    this.sub.next();
    this.sub.complete();
  }

}
