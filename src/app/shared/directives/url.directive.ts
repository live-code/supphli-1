import {Directive, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appUrl]'
})
export class UrlDirective {
  @Input('appUrl') url!: string ;

  @HostListener('click', ['$event'])
  clickMe(e: MouseEvent) {
    window.open(this.url)
  }

  ngOnInit() {
    if (!this.url) {
      throw new Error('this directive must define its url')
    }
  }
}
