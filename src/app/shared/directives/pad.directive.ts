import {Directive, HostBinding, Input} from '@angular/core';

@Directive({
  selector: '[appPad]',
})
export class PadDirective {
  @Input() appPad: number = 0;

  @HostBinding()  class = 'bg-red-500';

  @HostBinding('style.padding') get pad() {
    return this.appPad + 'px'
  }



}
