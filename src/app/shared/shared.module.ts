import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { MapquestComponent } from './components/mapquest.component';
import { CardComponent } from './components/card.component';
import { TabbarComponent } from './components/tabbar.component';
import { PadDirective } from './directives/pad.directive';
import { MarginDirective } from './directives/margin.directive';
import { UrlDirective } from './directives/url.directive';
import { ColorPickerComponent } from './cva/color-picker.component';
import { MyInputComponent } from './cva/my-input.component';
import {ReactiveFormsModule} from "@angular/forms";
import { IfLoggedDirective } from './directives/if-logged.directive';

@NgModule({
  declarations: [
    MapquestComponent,
    CardComponent,
    TabbarComponent,
    PadDirective,
    MarginDirective,
    UrlDirective,
    ColorPickerComponent,
    MyInputComponent,
    IfLoggedDirective
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [
    MapquestComponent,
    CardComponent,
    TabbarComponent,
    PadDirective,
    MarginDirective,
    UrlDirective,
    ColorPickerComponent,
    MyInputComponent,
    IfLoggedDirective
  ]
})
export class SharedModule {

}
