import {Component, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'app-color-picker',
  template: `

    <div class="flex gap-2">
      <div
        *ngFor="let c of colors"
        [style.background-color]="c"
        [style.width.px]="20"
        [style.height.px]="20"
        (click)="clickHandler(c)"
        [style.border]="value === c ? '4px solid black' : null"
      >
      </div>
    </div>
    {{value}}
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR ,
      multi: true,
      useExisting: ColorPickerComponent
    }
  ]
})
export class ColorPickerComponent implements ControlValueAccessor{
  @Input() colors: string[] = []
  value = ''
  onChange!: (color: string) => void;
  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(color: any): void {
    console.log('write value', color)
    this.value = color
  }

  clickHandler(c: string) {
    this.onTouched()
    this.onChange(c)
    this.value = c
  }
}
