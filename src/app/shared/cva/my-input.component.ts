import {Component, forwardRef, Injector, Input, Optional, Self, SkipSelf} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, NgControl,
  ValidationErrors,
  Validator
} from "@angular/forms";

const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;

@Component({
  selector: 'app-my-input',
  template: `
    <label for="">{{label}}</label>
    <input type="text"
           [formControl]="input"
           (blur)="onTouched()"
    >
    <div *ngIf="control.errors as err">
      <div *ngIf="err['required']">obbligatorio</div>
      <div *ngIf="err['minlength']">troppo corto</div>
      <div *ngIf="err['alphanumeric']">no symbols</div>
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MyInputComponent), multi: true
    },
    {
      provide: NG_VALIDATORS, useExisting: forwardRef(() => MyInputComponent), multi: true
    }
  ]
})
export class MyInputComponent implements ControlValueAccessor, Validator{
  @Input() label: string = '';
  @Input() alphaNumeric: boolean = false;
  input = new FormControl('')

  control!: NgControl;
  constructor(private inj: Injector) { }

  ngOnInit() {
     this.control = this.inj.get(NgControl)
    console.log(this.control)
  }
/*  constructor(@Self() @Optional() public control: NgControl) {
    this.control && (this.control.valueAccessor = this);
  }*/

  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.input.valueChanges.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(text: string): void {
    this.input.setValue(text)
  }

  validate(c: AbstractControl): ValidationErrors | null {
    console.log(c.errors)
    if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
      return { alphanumeric: true};
    }
    return null;
  }

}

