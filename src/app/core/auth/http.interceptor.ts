import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
} from '@angular/common/http';
import { Observable, withLatestFrom } from 'rxjs';

@Injectable()
export class HttpInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
   /* return this.auth....
    .pipe(
      withLatestFrom(...),

      map(([isLogged, token]) => {
        const clone = request;
        // ...
        return next.handle(clone);
      }


    )*/
    return next.handle(request);
  }
}


