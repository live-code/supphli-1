import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-navbar',
  template: `
   <div class="flex gap-4 m-4 flex-wrap ">
      <button class="btn" routerLink="login">login</button>
      <button class="btn" [routerLink]="'home'">home</button>
      <button class="btn" [routerLink]="'forms1'">forms1</button>
      <button class="btn" [routerLink]="'forms2'">forms2</button>
      <button class="btn" [routerLink]="'users'">users</button>
      <button class="btn" [routerLink]="'users'">users</button>
      <button class="btn" [routerLink]="'uikit'">uikit</button>
      <button class="btn" [routerLink]="'uikit2'">uikit2</button>
      <button class="btn" [routerLink]="'uikit3'">uikit3</button>
     <button class="btn" 
             *appIfLogged="'admin'"
             (click)="authSrv.logout()">Logout</button>

     <button class="btn" *appIfLogged="'admin'">
       CMS
     </button>
    </div>
  `,
  styles: [
  ]
})
export class NavbarComponent {
  constructor(public authSrv: AuthService) {
  }
}
