import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  BehaviorSubject, combineLatest,
  distinctUntilChanged,
  interval,
  map,
  ReplaySubject,
  share,
  shareReplay,
  startWith,
  Subject,
  take, withLatestFrom
} from 'rxjs';
import { Auth, Role } from '../model/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth$ = new BehaviorSubject<Auth | null>(null)
  data$ = new BehaviorSubject<any>(123)

  constructor(private http: HttpClient) {}

  login(user: string, pass: string) {
    const params = new HttpParams()
      .set('user', user)
      .set('pass', pass)

    this.http.get<Auth>('http://localhost:3000/login?username=', { params })
      .subscribe({
        next: (res) => {
          this.auth$.next(res)
        },
        error: (err) => {
          console.log(err)
        }
      })

  }

  logout() {
    this.auth$.next(null)
  }

  isLogged$() {
    return this.auth$
      .pipe(
        map(res => !!res),
        distinctUntilChanged()
      )
  }
 token$() {
    return this.auth$
      .pipe(
        map(res => res?.token),
      )
  }

  isRoleValid$(requiredRole: Role) {
    return this.auth$
      .pipe(
        map(res => res?.role === requiredRole),
        distinctUntilChanged()
      )
  }

  isValid$(requiredRole: Role) {
    return this.isLogged$()
      .pipe(
        withLatestFrom(
          combineLatest({
            isRoleValid: this.isRoleValid$(requiredRole),
            data: this.data$
          })
        ),
        map(([isLogged, obj]) => isLogged && obj.isRoleValid)
      )
  }
}
