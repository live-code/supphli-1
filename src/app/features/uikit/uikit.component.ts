import { Component } from '@angular/core';
interface City {
  id: number;
  name: string;
  desc: string;
}
interface Country {
  id: number;
  label: string;
  desc: string;
  cities: City[]
}
@Component({
  selector: 'app-uikit',
  template: `
    <app-tabbar
      [items]="countries"
      [active]="activeCountry"
      (tabClick)="countryTabClick($event)"
    ></app-tabbar>

    <app-tabbar
      [items]="activeCountry.cities"
      labelField="name"
      *ngIf="activeCountry"
      [active]="activeCity"
      (tabClick)="cityTabClick($event)"
    ></app-tabbar>

    <app-mapquest
      *ngIf="activeCity"
      [city]="activeCity.name" />

    <br>
    <br>
    <br>
    <br>
    <app-card
      title="My Profile"
      icon="fa fa-facebook"
      variant="primary"
      (iconClick)="openUrl('http://www.google.com')"
    >
      <h1 (click)="doSomething()">ciao ciao</h1>
    </app-card>

    <app-card
      [title]="1+1"
      icon="fa fa-google"
      url="http://www.google.com"
      (iconClick)="doSomething()"
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet assumenda corporis, dicta dolorem eius eum facilis fuga illum iusto magnam necessitatibus nulla placeat quis quos rem sequi unde vero voluptatibus.
    </app-card>


  `,
  styles: [
  ]
})
export class UikitComponent {
  countries: Country[] = []
  activeCountry: Country | undefined;
  activeCity: City | undefined;

  constructor() {
    setTimeout(() => {
      this.countries = [
        {
          id: 2,
          label: 'germany',
          desc: 'bla bla 2',
          cities: [
            { id: 1, name: 'Berlin', desc: '....' },
            { id: 2, name: 'Monaco', desc: '....' }
          ]
        },
        {
          id: 1,
          label: 'italy',
          desc: 'bla bla 1',
          cities: [
            { id: 11, name: 'Rome', desc: '....' },
            { id: 22, name: 'Milan', desc: '....' },
            { id: 33, name: 'Palermo', desc: '....' },
          ]
        },
        { id: 3, label: 'spain', desc: 'bla bla 3',
          cities: [
            {id: 41, name: 'Madrid', desc: '....'}
          ]},
      ];
      this.activeCountry = this.countries[0];
      this.activeCity = this.activeCountry.cities[0];

    }, 1000)
  }
  countryTabClick(country: Country) {
    this.activeCountry = country;
    this.activeCity = this.activeCountry.cities[0];
  }

  cityTabClick(city: City) {
    this.activeCity = city;
  }

  doSomething(item?: any) {
    console.log('tab click', item)
  }

  openUrl(url: string) {
      window.open(url)
  }
}
