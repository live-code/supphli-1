import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Uikit3Component } from './uikit3.component';

const routes: Routes = [{ path: '', component: Uikit3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Uikit3RoutingModule { }
