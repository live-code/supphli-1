import { AfterViewInit, Component, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Role } from '../../model/auth';

@Component({
  selector: 'app-uikit3',
  template: `
      
    <button *appIfLogged="'admin'">only for admins</button>
    
  `,
})
export class Uikit3Component  {

  constructor(private authService: AuthService) {

  }
}
