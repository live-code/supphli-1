import { Component } from '@angular/core';

@Component({
  selector: 'app-uikit2',
  template: `



    ciao <span appUrl="http://www.google.com">google </span>

    <div [appPad]="padValue" >MIAO</div>
    <div [appMargin]="padValue" [color]="color" ></div>

    <button class="btn" (click)="padValue = padValue+1">+</button>
    <hr>
    Open console:
    <button class="btn" (click)="color = 'purple'">purple</button>
    <button class="btn" (click)="color = 'cyan'">cyan</button>
  `,
})
export class Uikit2Component {
  padValue = 10;
  color = 'green';
}



