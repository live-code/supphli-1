import {Component, Input, Self} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../../model/user";
import {FormBuilder} from "@angular/forms";
import {UsersService} from "./services/users.service";
import {UsersStore} from "./services/users.store";

@Component({
  selector: 'app-users',
  template: `
    <app-users-forms
      [activeUser]="store.activeUser"
      (clear)="actions.clearActive()"
      (save)="actions.saveUser($event)"
    ></app-users-forms>

    <app-users-list
      [users]="store.users"
      [activeUser]="store.activeUser"
      (delete)="actions.deleteUser($event)"
      (setActive)="actions.setActiveUser($event)"
    ></app-users-list>
  `,
  providers: [
    UsersService, UsersStore
  ]
})
export class UsersComponent {
  constructor(
    public actions: UsersService,
    public store: UsersStore,
  ) {
    this.actions.getUsers()
  }

}
