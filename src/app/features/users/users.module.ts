import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { UsersListComponent } from './components/users-list.component';
import { UsersFormsComponent } from './components/users-forms.component';
import {ReactiveFormsModule} from "@angular/forms";
import { UsersListItemComponent } from './components/users-list-item.component';
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    UsersComponent,
    UsersListComponent,
    UsersFormsComponent,
    UsersListItemComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: '', component: UsersComponent },
    ])
  ],

})
export class UsersModule { }

