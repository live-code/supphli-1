import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from "../../../model/user";

@Component({
  selector: 'app-users-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `

    <app-users-list-item
      *ngFor="let u of users"
      [user]="u"
      [selected]="u.id === activeUser?.id"
      (delete)="delete.emit($event)"
      (setActive)="setActive.emit($event)"
    ></app-users-list-item>

  `,
})
export class UsersListComponent {
  @Input() users: User[] = [];
  @Input() activeUser: Partial<User> | undefined;
  @Output() delete = new EventEmitter<number>()
  @Output() setActive = new EventEmitter<User>()

}
