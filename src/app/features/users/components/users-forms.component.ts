import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {User} from "../../../model/user";

@Component({
  selector: 'app-users-forms',
  template: `
    <form [formGroup]="form"
          (submit)="save.emit(form.value)">
      <input type="text" formControlName="name" placeholder="your name">
      <button type="submit">
        <!--{{ _activeUser?.id ? 'EDIT' : 'ADD'}}-->
        SAVE
      </button>
      <button type="button" (click)="clearFormHandler()">clear</button>
    </form>
  `,
})
export class UsersFormsComponent {
  @Input() set activeUser(user: Partial<User> | undefined) {
    console.log(user)
    if (user?.id) {
      this.form.patchValue(user)
    } else {
      this.form.reset();
    }
  }
  @Output() save = new EventEmitter<any>();
  @Output() clear = new EventEmitter();

  form = this.fb.nonNullable.group({
    name: '',
  })

  constructor(private fb: FormBuilder) {}

  protected clearFormHandler() {
    this.form.reset()
    this.clear.emit()
  }

  resetHandler() {
    this.form.reset()
  }


}
