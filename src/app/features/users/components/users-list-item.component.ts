import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from "../../../model/user";

@Component({
  selector: 'app-users-list-item',
  template: `
    <li
      (click)="setActive.emit(user)"
      [ngClass]="{
        'active': selected
      }"
    >
      {{user.name}}
      <button (click)="deleteUser(user.id, $event)">Del</button>
      <button (click)="toggle($event)" *ngIf="user.address">Toggle</button>

      <div *ngIf="isOpen ">
        <app-mapquest
          [city]="user.address.city"
          [zoom]="zoom"
        ></app-mapquest>
        <button (click)="zoom = zoom - 1">-</button>
        <button (click)="zoom = zoom + 1">+</button>
      </div>
    </li>

  `,
  styles: [`
    .active {
      background: red;
    }
  `]
})
export class UsersListItemComponent {
  @Input() user!: User;
  @Input() selected: boolean = false;
  @Output() delete = new EventEmitter<number>()
  @Output() setActive = new EventEmitter<User>()
  isOpen = false;
  zoom = 8;

  deleteUser(id: number, e: MouseEvent) {
    e.stopPropagation()
    this.delete.emit(id)
  }

  toggle(e: MouseEvent) {
    e.stopPropagation()
    this.isOpen = !this.isOpen
  }
}
