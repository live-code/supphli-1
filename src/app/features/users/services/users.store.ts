import {Injectable} from "@angular/core";
import {User} from "../../../model/user";

@Injectable()
export class UsersStore {
  users: User[] = [];
  activeUser: Partial<User> = {}

  init(users: User[]) {
    this.users = users;
  }

  add(user: User) {
    this.users = [...this.users, user]
    this.clearActive();
  }

  edit(user: User) {
    this.users = this.users.map(u => {
      return u.id === this.activeUser.id ? user  : u
    })
  }

  delete(id: number) {
    this.users = this.users.filter(u => u.id !== id)

    if (this.activeUser?.id === id) {
      this.clearActive();
    }
  }

  setActiveUser(u: User) {
    this.activeUser = u;
  }

  clearActive() {
    this.activeUser = {};
  }

}
