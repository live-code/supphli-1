import { Injectable } from '@angular/core';
import {User} from "../../../model/user";
import {HttpClient} from "@angular/common/http";
import {UsersStore} from "./users.store";


export type Actions = 'add' | 'edit';
export type ActionType = { action: Actions }

@Injectable()
export class UsersService {
  constructor(
    private http: HttpClient,
    private store: UsersStore
  ) {
  }

  getUsers() {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.store.init(res)
      })
  }

  deleteUser(id: number) {
      this.http.delete<void>(`https://jsonplaceholder.typicode.com/users/${id}`)
        .subscribe(() => this.store.delete(id))
  }

  saveUser(formData: Partial<User>)  {
    if (this.store.activeUser?.id) {
      return this.editUser(formData)
    } else {
      return this.addUser(formData);
    }
  }

  addUser(formData: any) {
      this.http.post<User>(`https://jsonplaceholder.typicode.com/users/`, formData)
        .subscribe(res =>  this.store.add(res))
  }

  editUser(formData: any) {
      this.http.patch<User>(`https://jsonplaceholder.typicode.com/users/${this.store.activeUser?.id}`, formData)
        .subscribe(res => this.store.edit(res))
  }

  setActiveUser(u: User) {
    this.store.setActiveUser(u);
  }

  clearActive() {
    this.store.clearActive()
  }

}
