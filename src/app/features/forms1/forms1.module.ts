import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Forms1RoutingModule } from './forms1-routing.module';
import { Forms1Component } from './forms1.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    Forms1Component
  ],
  imports: [
    CommonModule,
    Forms1RoutingModule,
    ReactiveFormsModule
  ]
})
export class Forms1Module { }
