import { Component } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {catchError, concatMap, debounceTime, delay, filter, map, mergeAll, mergeMap, of, switchMap} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-forms1',
  template: `
    <form [formGroup]="form">
      <input type="checkbox" formControlName="isCompany">
      isCompany
      <hr>
      <input type="text" formControlName="name" placeholder="name">
      <input type="text"
             formControlName="vat"
             [placeholder]="form.get('isCompany')?.value ? 'VAT' : 'CF'"
      >

      <div *ngIf="form.get('name')?.errors as err">
        {{err['required'] }}
      </div>
    </form>

    <pre>{{form.value | json}}</pre>
  `,
  styles: [
  ]
})
export class Forms1Component {
  form = this.fb.nonNullable.group({
    isCompany: true,
    name: ['', Validators.required],
    vat: [0, Validators.minLength(11)]
  })

  constructor(private fb: FormBuilder, private http: HttpClient) {
    // this.form.get('name')?.disable()

    this.form?.get('name')?.valueChanges
      ?.pipe(
        filter(text => text.length > 3),
        switchMap(
          (text) => this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
            .pipe(
              catchError(err => {
                return of(null)
              })
            )
        ),

      )
      .subscribe(meteo => {
        console.log(meteo)
      })


  }
}
