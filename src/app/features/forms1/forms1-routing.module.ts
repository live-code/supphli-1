import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Forms1Component } from './forms1.component';

const routes: Routes = [{ path: '', component: Forms1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Forms1RoutingModule { }
