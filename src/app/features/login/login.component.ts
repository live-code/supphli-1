import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-login',
  template: `
    <p>
      login works!
    </p>
    
    <input type="text">
    <input type="text">
    
    <button (click)="loginHandler()">
       SIGN IN
    </button>
  `,
})
export class LoginComponent {
  sub!: Subscription
  constructor(private authSrv: AuthService) {
  }

  loginHandler() {
    this.authSrv.login('fabio', '123');

  }

  ngOnDestroy() {

  }
}
