import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { AsyncUserValidatorService } from '../../shared/forms/async-user-validator.service';

@Component({
  selector: 'app-forms2',
  template: `
    <form [formGroup]="form">
      pending:           {{form.get('anagrafica.name')?.pending}}
      valid:           {{form.get('anagrafica.name')?.valid}}

      <hr>
      <div formGroupName="anagrafica">
        <div>
          
          <anagrafica formControlName="anagrafica"></anagrafica>
          <h1 class="text-xl">
            <i class="fa fa-check" *ngIf="form.get('anagrafica')?.valid"></i>
            ANAGRAFICA
          </h1>
          <app-my-input
            label="Your Name"
            [alphaNumeric]="true"
            formControlName="name"></app-my-input>
          
          </div>
          <div>{{form.get('anagrafica.name')?.valid}}</div>
        
          <div>
          <app-my-input
            label="Your Surname"
            [alphaNumeric]="true"
            formControlName="surname"></app-my-input>
          </div>
      </div>
        
      <app-color-picker
        formControlName="color"
        [colors]="['brown', 'cyan', 'green']"
      ></app-color-picker>
      
      <div formGroupName="passwords">
        <h1 class="text-xl">PASSWORDS</h1>
        {{form.get('passwords')?.errors | json}}
        {{form.get('passwords.pass2')?.errors | json}}
        <br>
        <input type="text" formControlName="pass1" placeholder="pass">
        <input type="text" formControlName="pass2" placeholder="confirm pass">
      </div>
      <!--{{form.get('pass2').errors.passNOtMatch}}-->

      <button class="btn" type="submit" 
              [disabled]="form.invalid || form.pending 
">SAVE</button>
      <hr>
      <pre>{{form.value | json}}</pre>
      <pre>{{form.valid | json}}</pre>
    </form>
  `,
})
export class Forms2Component {
  form = this.fb.nonNullable.group({
    anagrafica: this.fb.group({
      name: [
        'mario',
        [Validators.required, Validators.minLength(3) ],
        [this.validatorUserAsync.checkUniqueUsername()]
      ],
      surname: ['fabio',[
        Validators.required, Validators.minLength(3)
      ]],
    }),
    passwords: this.fb.group(
      {
        pass1: ['', Validators.required],
        pass2: ['', Validators.required]
      },
      {
        validators: passwordMatch('pass1', 'pass2')
      }
    ),
    color: ['', Validators.required],
  }, {
    // updateOn: 'blur'
  })

  constructor(
    private fb: FormBuilder,
    private validatorUserAsync: AsyncUserValidatorService
  ) {

/*    setTimeout(() => {
      const res = {
        anagrafica: {
          name: 'biondi',
          surname: 'y'
        },
        color: ''
      }
      this.form.patchValue(res, {emitEvent: false})
    }, 2000)*/
  }
}

export function passwordMatch(key1: string, key2: string): ValidatorFn {
  return (group: AbstractControl) => {
    const p1 = group.get(key1);
    const p2 = group.get(key2);

    if (p1?.value !== p2?.value) {
      // group.get('pass2')?.setErrors({ passWrong: true})
      return { passNotMatch: true }
    } else {
      //group.get('pass2')?.setErrors({ ...group.get('pass2')?.errors })
    }

    return null;
  }
}
