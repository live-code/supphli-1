import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Forms2RoutingModule } from './forms2-routing.module';
import { Forms2Component } from './forms2.component';
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    Forms2Component
  ],
  imports: [
    CommonModule,
    Forms2RoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class Forms2Module { }
