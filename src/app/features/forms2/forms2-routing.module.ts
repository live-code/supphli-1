import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Forms2Component } from './forms2.component';

const routes: Routes = [{ path: '', component: Forms2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Forms2RoutingModule { }
